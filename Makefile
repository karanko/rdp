

.PHONY: all clean install

all:
	gcc -L/usr/X11R6/lib -lX11 rdp.c -o rdp 
clean:
	rm rdp
install:
	chmod +x rdp
	mkdir -p ~/bin
	cp rdp ~/bin