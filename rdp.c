#include <stdio.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/wait.h>
#include <X11/Xlib.h>
#include <X11/Xutil.h>

#define START_WIDTH 800
#define START_HEIGHT 600

int done = 0;
int resizing = 0;
pid_t pid = 0;
Window mywindow;
Display *mydisplay;
void cleanup_and_exit(int code)
{
    kill(pid, SIGKILL);
    done = 1;
    XUnmapWindow(mydisplay, mywindow);
    XDestroyWindow(mydisplay, mywindow);
    XCloseDisplay(mydisplay);
    exit(code);
}
void sigchldHandler(int sig)
{

    signal(SIGCHLD, sigchldHandler); //reset handler to catch SIGCHLD for next time;
    int status;
    pid_t _pid;

    _pid = wait(&status); //After wait, child is definitely freed.
    printf("pid = %d , status = %d\n", _pid, status);
    pid = 0;
    if (resizing == 0)
    {
        cleanup_and_exit(0);
    }
}

int main(int argc, char *argv[])
{

    signal(SIGCHLD, sigchldHandler);

    int opt;
    char *window_name = "rdp";
    char *icon_name = "RD";
    int userpresent = 0;
    char *user;
    char *password;
    int passwordpresent = 0;

    unsigned long valuemask = CWBackPixel | CWBorderPixel | CWEventMask;
    ;
    mydisplay = XOpenDisplay("");
    int screen_num = DefaultScreen(mydisplay);

    XSetWindowAttributes myat;
    myat.background_pixel = WhitePixel(mydisplay, screen_num);
    myat.border_pixel = BlackPixel(mydisplay, screen_num);
    myat.event_mask = StructureNotifyMask;

    mywindow = XCreateWindow(mydisplay, RootWindow(mydisplay, screen_num),
                             0, 0, START_WIDTH, START_HEIGHT, 0,
                             DefaultDepth(mydisplay, screen_num), InputOutput,
                             DefaultVisual(mydisplay, screen_num),
                             valuemask, &myat);
    XTextProperty windowName, iconName;
    XStringListToTextProperty(&window_name, 1, &windowName);
    XSetWMName(mydisplay, mywindow, &windowName);
    XStringListToTextProperty(&icon_name, 1, &iconName);
    XSetWMIconName(mydisplay, mywindow, &iconName);
    Atom wm_delete_window = XInternAtom(mydisplay, "WM_DELETE_WINDOW", False);
    XSetWMProtocols(mydisplay, mywindow, &wm_delete_window, 1);

    XWMHints wmhints;
    wmhints.initial_state = NormalState;
    wmhints.flags = StateHint;
    XSetWMHints(mydisplay, mywindow, &wmhints);

    XMapWindow(mydisplay, mywindow);

    XEvent myevent;

    int target_h = START_HEIGHT;
    int target_w = START_WIDTH;
    int current_h = START_HEIGHT;
    int current_w = START_WIDTH;

    char *argr[(argc + 1)];
    for (int i = 0; i < argc; i++)
    {
        argr[i + 2] = argv[i + 1];
    }
    char geo[15];
    sprintf(geo, "-g%dx%d", current_w, current_h);
    argr[0] = &geo[15];
    char windowid[15];
    sprintf(windowid, "-X%d", mywindow);
    argr[1] = &windowid[15];

    pid = vfork();
    if (pid == 0)
    {
        execvp("rdesktop", argr);
    }

    while (done == 0)
    {
        XNextEvent(mydisplay, &myevent);
        switch (myevent.type)
        {
        case DestroyNotify:
            printf("DestroyNotify\n");
            cleanup_and_exit(0);
        case ConfigureNotify:
            printf("ConfigureNotify\n");

            target_h = myevent.xconfigure.height;
            target_w = myevent.xconfigure.width;
            if (pid > 0 && target_h == current_h && target_w == current_w)
            {
                continue;
            }
            resizing = 1;
            current_h = target_h;
            current_w = target_w;
            sprintf(geo, "-g%dx%d", current_w, current_h);
            argr[0] = &geo[15];
            if (pid > 0)
            {
                kill(pid, SIGKILL);
                waitpid(pid, NULL, 0);
                usleep(50000);
                pid = 0;
            }

            pid = vfork();
            if (pid == 0)
            {
                execvp("rdesktop", argr);
            }
            resizing = 0;
            break;
        case ClientMessage:
            printf("ClientMessage\n");

            if ((Atom)myevent.xclient.data.l[0] == wm_delete_window)
            {
                printf("wm_delete_window\n");

                cleanup_and_exit(0);
            }
            break;
        }
    }
    cleanup_and_exit(0);
}
